# qotd

A QOTD discord integration. It allows any whitelisted role, to publish QOTD messages in a specified Discord channel.

It will require the user to login with Discord to authenticate